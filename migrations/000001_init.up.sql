CREATE TABLE statistics 
(
    id UUID PRIMARY KEY,
    stat_date date not null,
    views integer,
    clicks integer,
    cost numeric(2),
    cpc numeric(2),
    cpm numeric(2)
)