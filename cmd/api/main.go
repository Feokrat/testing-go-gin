package main

import "gitlab.com/Feokrat/testing-go-gin/internal/app"

const configPath = "configs/config"

func main() {
	app.Run(configPath)
}
