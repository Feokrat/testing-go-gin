package service

import (
	"time"

	"gitlab.com/Feokrat/testing-go-gin/internal/models"
	"gitlab.com/Feokrat/testing-go-gin/internal/repository"
)

type Statistic interface {
	AddStatistic(statistic models.Statistic) error
	GetStatisticsByDates(from time.Time, to time.Time) ([]models.Statistic, error)
	GetAllStatistics() ([]models.Statistic, error)
	DeleteAll() error
}

type Services struct {
	Statistic Statistic
}

type Dependencies struct {
	Repositories *repository.Repositories
}

func NewServices(deps Dependencies) *Services {
	statisticService := NewStatisticService(deps.Repositories.Statistic)

	return &Services{statisticService}
}
