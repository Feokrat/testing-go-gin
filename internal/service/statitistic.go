package service

import (
	"errors"
	"time"

	"gitlab.com/Feokrat/testing-go-gin/internal/models"
	"gitlab.com/Feokrat/testing-go-gin/internal/repository"
)

type StatisticService struct {
	repo repository.Statistic
}

func NewStatisticService(repo repository.Statistic) *StatisticService {
	return &StatisticService{repo: repo}
}

func (s *StatisticService) AddStatistic(statistic models.Statistic) error {
	if err := s.repo.Create(statistic); err != nil {
		return err
	}
	return nil
}

func (s *StatisticService) GetStatisticsByDates(dateFrom time.Time, dateTo time.Time) ([]models.Statistic, error) {
	if dateFrom.After(dateTo) {
		return nil, errors.New("fromTime is after toTime")
	}
	statistics, err := s.repo.GetByDate(dateFrom, dateTo)
	if err != nil {
		return nil, err
	}
	return statistics, nil
}

func (s *StatisticService) GetAllStatistics() ([]models.Statistic, error) {
	statistics, err := s.repo.GetAll()
	if err != nil {
		return nil, err
	}
	return statistics, nil
}

func (s *StatisticService) DeleteAll() error {
	if err := s.repo.DeleteAll(); err != nil {
		return err
	}
	return nil
}
