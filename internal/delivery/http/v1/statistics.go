package v1

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/Feokrat/testing-go-gin/internal/models"
)

type inputStatistic struct {
	Date   models.CustomTime `json:"stat_date" db:"stat_date" binding:"required" format:"2006-01-02"`
	Views  uint32            `json:"views" db:"views"`
	Clicks uint32            `json:"clicks" db:"clicks"`
	Cost   float32           `json:"cost" db:"cost"`
}

func (h *Handler) initStatisticRoutes(api *gin.RouterGroup) {
	statistics := api.Group("/statistics")
	{
		// statistics.GET("", h.getAllStatistics)
		statistics.GET("", h.getStatisticsByDate)
		statistics.POST("", h.addStatistic)
		statistics.DELETE("", h.deleteAllStatistic)
	}
}

// func (h *Handler) getAllStatistics(ctx *gin.Context) {
// 	statistics, err := h.services.Statistic.GetAllStatistics()
// 	if err != nil {
// 		newResponse(ctx, http.StatusInternalServerError, err.Error())
// 		log.Print("getall", err)
// 		return
// 	}

// 	ctx.JSON(http.StatusOK, dataResponse{statistics})
// }

func (h *Handler) getStatisticsByDate(ctx *gin.Context) {
	dateFrom, err := getFromDate(ctx)
	if err != nil {
		newResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	dateTo, err := getToDate(ctx)
	if err != nil {
		newResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	statistics, err := h.services.Statistic.GetStatisticsByDates(dateFrom, dateTo)
	if err != nil {
		newResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, dataResponse{statistics})
}

func (h *Handler) addStatistic(ctx *gin.Context) {
	var newStat inputStatistic
	if err := ctx.BindJSON(&newStat); err != nil {
		newResponse(ctx, http.StatusBadRequest, "Invalid input body")
		return
	}

	stat := models.Statistic{
		ID:     uuid.New(),
		Date:   newStat.Date,
		Views:  newStat.Views,
		Clicks: newStat.Clicks,
		Cost:   newStat.Cost,
	}

	if newStat.Clicks != 0 {
		stat.Cpc = newStat.Cost / float32(newStat.Clicks)
	}
	if newStat.Views != 0 {
		stat.Cpm = newStat.Cost / float32(newStat.Views) * 1000
	}

	if err := h.services.Statistic.AddStatistic(stat); err != nil {
		newResponse(ctx, http.StatusInternalServerError, "Couldn't create statistic.")
		return
	}

	ctx.JSON(http.StatusOK, stat)
}

func (h *Handler) deleteAllStatistic(ctx *gin.Context) {
	if err := h.services.Statistic.DeleteAll(); err != nil {
		newResponse(ctx, http.StatusInternalServerError, "Couldn't delete statistic.")
		return
	}

	ctx.JSON(http.StatusOK, HTTPResponse{
		Message: "Ok",
	})
}
