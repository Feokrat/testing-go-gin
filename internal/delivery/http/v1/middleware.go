package v1

import (
	"errors"
	"time"

	"github.com/gin-gonic/gin"
)

const (
	dateFrom = "from"
	dateTo   = "to"
)

func getFromDate(ctx *gin.Context) (time.Time, error) {
	query := ctx.Request.URL.Query()
	fromDate, ok := query[dateFrom]
	if !ok {
		return time.Time{}, errors.New("fromDate is invalid type")
	}

	res, err := time.Parse("2006-01-02", fromDate[0])
	if err != nil {
		return time.Time{}, err
	}

	return res, nil
}

func getToDate(ctx *gin.Context) (time.Time, error) {
	toDateCtx := ctx.Request.URL.Query()
	toDate, ok := toDateCtx[dateTo]
	if !ok {
		return time.Time{}, errors.New("toDate is invalid type")
	}

	res, err := time.Parse("2006-01-02", toDate[0])
	if err != nil {
		return time.Time{}, err
	}

	return res, nil
}
