package v1

import (
	"log"

	"github.com/gin-gonic/gin"
)

type HTTPResponse struct {
	Message string `json:"message"`
}

type dataResponse struct {
	Data interface{} `json:"data"`
}

func newResponse(c *gin.Context, statusCode int, message string) {
	log.Print(message)
	c.AbortWithStatusJSON(statusCode, HTTPResponse{message})
}
