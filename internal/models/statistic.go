package models

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
)

type Statistic struct {
	ID     uuid.UUID  `json:"id" db:"id"`
	Date   CustomTime `json:"stat_date" db:"stat_date" binding:"required" format:"2006-01-02"`
	Views  uint32     `json:"views" db:"views"`
	Clicks uint32     `json:"clicks" db:"clicks"`
	Cost   float32    `json:"cost" db:"cost"`
	Cpc    float32    `json:"cpc" db:"cpc"`
	Cpm    float32    `json:"cpm" db:"cpm"`
}

type CustomTime struct {
	time.Time
}

const ctLayout = "2006-01-02"

func (ct *CustomTime) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), "\"")
	if s == "null" {
		ct.Time = time.Time{}
		return
	}
	ct.Time, err = time.Parse(ctLayout, s)
	return
}

func (ct *CustomTime) MarshalJSON() ([]byte, error) {
	if ct.Time.UnixNano() == nilTime {
		return []byte("null"), nil
	}
	return []byte(fmt.Sprintf("\"%s\"", ct.Time.Format(ctLayout))), nil
}

var nilTime = (time.Time{}).UnixNano()

func (ct *CustomTime) IsSet() bool {
	return ct.UnixNano() != nilTime
}

func (ct *CustomTime) Scan(src interface{}) error {
	var value time.Time
	switch src.(type) {
	case time.Time:
		value = src.(time.Time)
	default:
		return errors.New("invalid type for ExpireTimestamp")
	}
	*ct = CustomTime{Time: value}
	return nil
}
