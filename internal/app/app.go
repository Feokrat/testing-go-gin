package app

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/Feokrat/testing-go-gin/internal/config"
	"gitlab.com/Feokrat/testing-go-gin/internal/database/postgresql"
	"gitlab.com/Feokrat/testing-go-gin/internal/delivery/http"
	"gitlab.com/Feokrat/testing-go-gin/internal/repository"
	"gitlab.com/Feokrat/testing-go-gin/internal/server"
	"gitlab.com/Feokrat/testing-go-gin/internal/service"
)

func Run(configPath string) {
	cfg, err := config.Init(configPath)
	if err != nil {
		log.Print(err)
		return
	}

	db, err := postgresql.NewPostgresDB(cfg.Postgresql)
	if err != nil {
		log.Print(err)
		return
	}

	repositories := repository.NewRepositories(db)
	services := service.NewServices(service.Dependencies{Repositories: repositories})
	handlers := http.NewHandler(services)

	server := server.NewServer(cfg, handlers.Init(cfg.HTTP.Host, cfg.HTTP.Port))
	go func() {
		if err := server.Run(); err != nil {
			log.Printf("error occurred while running http server: %s\n", err.Error())
		}
	}()

	log.Print("Server started")

	// Graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)

	// Block until a signal is received.
	sig := <-c
	log.Println("Got signal:", sig)
	log.Print("Shutting down server")

	ctx, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	server.Stop(ctx)
}
