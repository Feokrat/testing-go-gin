package repository

import (
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/Feokrat/testing-go-gin/internal/models"
)

type StatisticRepositoryPostgres struct {
	db *sqlx.DB
}

func NewStatisticRepositoryPostgres(db *sqlx.DB) *StatisticRepositoryPostgres {
	return &StatisticRepositoryPostgres{db}
}

func (r *StatisticRepositoryPostgres) Create(statistic models.Statistic) error {
	if err := r.db.QueryRow("INSERT INTO statistics (id, stat_date, clicks, cost, views, cpc, cpm) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id",
		statistic.ID, statistic.Date.Time, statistic.Clicks, statistic.Cost, statistic.Views, statistic.Cpc, statistic.Cpm).Scan(&statistic.ID); err != nil {
		log.Print(err)
		return err
	}

	return nil
}

func (r *StatisticRepositoryPostgres) GetByDate(from time.Time, to time.Time) ([]models.Statistic, error) {
	var statistics []models.Statistic
	query := "SELECT s.id, s.stat_date, s.clicks, s.cost, s.views, s.cpc, s.cpm FROM statistics AS s WHERE s.stat_date BETWEEN $1 AND $2"

	if err := r.db.Select(&statistics, query, from, to); err != nil {
		return nil, err
	}
	return statistics, nil
}

func (r *StatisticRepositoryPostgres) GetAll() ([]models.Statistic, error) {
	var statistics []models.Statistic
	query := "SELECT s.id, s.stat_date, s.clicks, s.cost, s.views, s.cpc, s.cpm FROM statistics AS s"

	if err := r.db.Select(&statistics, query); err != nil {
		return nil, err
	}
	return statistics, nil
}

func (r *StatisticRepositoryPostgres) DeleteAll() error {
	_, err := r.db.Exec("DELETE FROM statistics")
	if err != nil {
		return err
	}

	return nil
}
