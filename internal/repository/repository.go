package repository

import (
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/Feokrat/testing-go-gin/internal/models"
)

type Statistic interface {
	Create(statistic models.Statistic) error
	GetByDate(from time.Time, to time.Time) ([]models.Statistic, error)
	GetAll() ([]models.Statistic, error)
	DeleteAll() error
}

type Repositories struct {
	Statistic Statistic
}

func NewRepositories(db *sqlx.DB) *Repositories {
	return &Repositories{
		Statistic: NewStatisticRepositoryPostgres(db),
	}
}
