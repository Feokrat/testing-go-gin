package repository_test

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Feokrat/testing-go-gin/internal/config"
	"gitlab.com/Feokrat/testing-go-gin/internal/models"
	"gitlab.com/Feokrat/testing-go-gin/internal/repository"
	"os"
	"testing"
	"time"
)

var (
	dbConfig config.PGConfig
)

func TestMain(m *testing.M) {
	dbConfig.Username = "postgres"
	dbConfig.Password = "postgres"
	dbConfig.Host = "localhost"
	dbConfig.Port = "5432"
	dbConfig.DBName = "go_gin_test"
	dbConfig.SSLMode = "disable"
	os.Exit(m.Run())
}

func TestStatisticRepositoryPostgres_Create(t *testing.T) {
	db, teardown := repository.TestDb(t, dbConfig)
	defer teardown("statistics")
	repo := repository.NewRepositories(db)

	s := models.Statistic{
		ID:     uuid.New(),
		Date:   models.CustomTime{Time: time.Now()},
		Views:  1,
		Clicks: 1,
		Cost:   0,
		Cpc:    0,
		Cpm:    0,
	}

	err := repo.Statistic.Create(s)

	assert.NoError(t, err)

	err = repo.Statistic.Create(s)
	assert.Error(t, err)
}

func TestStatisticRepositoryPostgres_GetAll(t *testing.T) {
	db, teardown := repository.TestDb(t, dbConfig)
	defer teardown("statistics")
	repo := repository.NewRepositories(db)

	result, err := repo.Statistic.GetAll()

	assert.NoError(t, err)
	assert.Empty(t, result)
}

func TestStatisticRepositoryPostgres_DeleteAll(t *testing.T) {
	db, teardown := repository.TestDb(t, dbConfig)
	defer teardown("statistics")
	repo := repository.NewRepositories(db)

	err := repo.Statistic.DeleteAll()

	assert.NoError(t, err)
}

func TestStatisticRepositoryPostgres_GetByDate(t *testing.T) {
	db, teardown := repository.TestDb(t, dbConfig)
	defer teardown("statistics")
	repo := repository.NewRepositories(db)

	result, err := repo.Statistic.GetByDate(time.Now().AddDate(1,0,0), time.Now())

	assert.Empty(t, result)
	assert.NoError(t, err)
}